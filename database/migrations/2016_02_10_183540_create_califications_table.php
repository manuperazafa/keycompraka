<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
       Schema::create('califications', function (Blueprint $table) { 
        $table->increments('id');
        $table->string('user_id');
        $table->string('user_target_id');
        $table->string('comentario');
        $table->string('calificacion');
        $table->timestamps();
       }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
