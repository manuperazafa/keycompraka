<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('cedula', 60);
            $table->string('zona_postal', 60);
            $table->string('genero', 60);
            $table->string('googleId');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('logo');
            $table->string('twitterId');
            $table->string('twitterUserName');  
            $table->string('facebookId');
            $table->string('instagramId');
            $table->string('instagramAccesToken');
            $table->string('instagramUserName');
            $table->string('user_type')->default(2);
            $table->boolean('confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
