@extends('layouts.app')

@section('content')
<div class="container" style="background-color : white; margin-top : 40px;  border-radius: 5px;">
  <h2><a href="{{url('/newTag')}}">Añadir nueva</a></h2>
  <h2>Categorias</h2>

   <table class="table">
    <thead>
      <tr>
        <th>Nombre</th>
        <th colspan="2">Acciones</th>

      </tr>
    </thead>
    <tbody>
      
     @foreach ($tag as $key => $tags)
							
							<tr>
				       
				        <td>{{$tags->name}}</td>
				        <td><a href="{{url('/editTag')}}/{{$tags->id}}">Editar</a></td>
				        <td><a href="{{url('/deleteTag')}}/{{$tags->id}}">Eliminar</a></td>
         
             </tr>
						@endforeach	
    </tbody>
  </table>
</div>


@endsection						