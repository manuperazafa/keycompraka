@extends('layouts.app')

@section('content')
<div class="content">
			
	<section>
        <!--info-user-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12" style="color:white;">
                    <div class="col-sm-4">
                    <div class="inline">
                    
                    <div class="inline" style="    float: left;background-image : url('{{$user->profile_picture}}'); height : 120px ; width:120px; background-size: 100% 100%; border-radius : 100px;"></div>
                  
                    
                    <div class="inline" id="detalle1" style="float : left;text-align: center;height:80px;       border-radius: 5px; padding-top:10px;"><h4 style="margin-top : 0;">{{$user->name}}</h4>
                         <input-stars max="5" ng-model="promedio"  ng-init="promedio = {{$user->promedio}}" readonly ></input-stars>
                      </div>
                    </div>
                    </div>
                    <div class="col-sm-2">
                    <div id="detalle2" style="text-align: center;height:80px;     background-color: #555; border-radius: 5px; padding-top:10px;">
                         <h4 style="    margin-top: 0px;
">{{$user->followers}}</h4> <h4>Seguiendo</h4></div>
                    </div>
                    <div class="col-sm-2">  
                    <div  id="detalle2" style="text-align: center;height:80px;     background-color: #555; border-radius: 5px; padding-top:10px;">
                         <h4 style="    margin-top: 0px;
">{{$user->followings}}</h4> <h4>Seguidores</h4>
                    </div>
                    </div>
                    
                    <div class="col-sm-3">
                    <div id="detalle2" style="text-align: center;height:80px;     background-color: #555; border-radius: 5px; padding-top:10px;"><div class="inline col-sm-8"><h4 style="margin-top: 5px;">Vendedor</h4> <h4>Compraka</h4></div><div class="inline col-sm-4"><h4 style="margin-top: 0px;"><i class="fa fa-star inline fa-3x" style="color : yellow;"></i></h4></div></div>
                    </div>
                </div>
                
                
            </div>
        </div>
        <!--/info-user-->
    </section>
    <section id="section-3">
        <!--Publicaciones-Comentarios-->
        <div class="container">
            <div class="row">
                <div class="col-xs-6 middle-p-c text-center" id="w-48">
                    <button type="button" class="btn btn-default w-100 w-105 no-b-r b-c-b" ng-click="tab = 1" style="height: 75px;">Publicaciones</button>
                </div>
                <div class="col-xs-6 text-center">
                    <button type="button" class="btn btn-default w-100 w-105 b-c-b no-b-r" ng-click="tab = 2" style="height: 75px;">Comentarios</button>
                </div>
            </div>
        </div>
    </section>
    <!--/Publicaciones-Comentarios-->
    <div ng-show="tab === 1">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <!--features_items-->
                        @foreach ($data as $key => $media)
                        <a href="{{$data[$key]['link']}}" target="_blank">			
                        <div class="col-sm-2" style="width : 24%;border-radius:5px; margin:5px;    padding: 10px;    background: white;">
                            <div class="product-image-wrapper" style="margin:0;border-left : 0; border-right : 0; border-bottom : 0px;     border: 0px;">
                                <div class="single-products" >
                                    <div class="productinfo text-center" style="background-image : url('{{$data[$key]['images']['standard_resolution']['url']}}');background-size: 100% 100%;
    background-repeat: no-repeat;">
                                        <i class="" id="fauser" style="background-color: transparent; display: inline-block; ') ; background-size : cover;"></i>
                                      
                                        
                                      
                                          
                                      </div>
                                </div>
                                
                            </div>
                        </div>
                        </a>
                       	@endforeach
                        <!--features_items-->
                    </div>
                </div>
            </div>
    </section>
    </div>
     <div ng-show="tab === 2">
         <section><!--comentario-focus-->
      @if (Auth::guest())
                @else      
        <div class="container">

            <div class="row">
                
                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <!--features_comments-->
                        <div class="b-yellow">
                        <i class="fa fa-comment pull-left" id="facomment1"></i>
                            <div class="form-group">
                            <textarea class="form-control" rows="5" ng-model="calificacion.comentario" id="comment1" placeholder="Escribe tu comentario..." style="">@if ($MyCalification) {{$MyCalification->comentario}} @endif</textarea>                              
                            </div>
                        </div>
                        <!--features_comments-->
                    </div>
                </div>

                <div class="pull-right" id="btn-m-r">
                    @if ($MyCalification)
                    <button type="button" class="btn btn-default get" id="calification-btn" ng-click="calificar({{$user->id}})">Actualizar Calificacion</button>
              
                    @else 
                    <button type="button" class="btn btn-default get" id="calification-btn" ng-click="calificar({{$user->id}})">Calificar</button>
                    @endif
                </div>
                <div class="pull-right" id="btn-m-r">
                    
                        <input-stars max="5" icon-base="fa fa-fw fa-3x" icon-empty="fa-star-o" icon-hover="hover" icon-full="fa-star" ng-model="calificacion.calificacion" ></input-stars>
                </div>    
            </div>
           
        </div>  
       @endif    
    </section>
      <section><!--comentarios-list-->
            <div class="container">
                 
                
                <div class="row" ng-init="getCalifications({{$user->id}})" >
                       <div class="b-white w-99" ng-repeat="cfclist in calificacionList | orderBy:cfclist">
                           <i class="fa fa-user pull-left" id="facomments"></i><p class="font-size-17-2"><% cfclist.username %> a las <% cfclist.updated_at | amDateFormat:'dddd, MMMM Do YYYY, h:mm:ss a' %></p>
                           <i class="fa fa-comment pull-left" id="facomment-2"></i><p class="font-size-17-1"><% cfclist.comentario %> </p>

                           
                           
                           <input-stars max="5" ng-model="ca.property"  ng-init="ca.property = cfclist.calificacion" readonly ><%  %></input-stars>
                       </div>

                       
                </div> 
                x 
            </div>
    </section><!--/comentarios-list-->

</div>    
    
@endsection		



                   
                     
