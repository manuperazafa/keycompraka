@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Social Networks</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updateprofile') }}">
                        {!! csrf_field() !!}

                        

                        <div class="form-group">
                            <label class="col-md-4 control-label">Instagram</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['instagramId']}}">
                                <span class = "input-group-addon"><a href="{{ url('/addInstagram') }}"  ><i class="fa  fa-instagram"></i></a></span>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Twitter</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['twitterId']}}">
                                <span class = "input-group-addon"><a href="{{ url('/addTwitter') }}"  ><i class="fa  fa-twitter"></i></a></span>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Facebook</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['facebookId']}}">
                                <span class = "input-group-addon"><a href="{{ url('/loginFacebook') }}"  ><i class="fa  fa-facebook"></i></a></span>
                                </div>
                                
                            </div>
                        </div>

                      
                        

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Save
                                </button>
                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
