@extends('layouts.app')

@section('content')
<div class="container" style="background-color : white; margin-top : 40px;  border-radius: 5px;">
  <h2>Comentarios</h2>
   <table class="table">
    <thead>
      <tr>
        <th>Nombre de usuario</th>
        <th>Comentario</th>
        <th>Calificacion</th>


      </tr>
    </thead>
    <tbody>
      
     @foreach ($data as $key => $repu)
							
							<tr>
				        <td>{{$repu->comentario}}</td>
				        <td>{{$repu->instagramUserName}}</td>
                <td>{{$repu->ncalificacion}}</td>
                
				        <td><a href="{{url('/editCalification')}}/{{$repu->idComment}}">Editar</a></td>
				      </tr>
						@endforeach	
    </tbody>
  </table>
</div>


@endsection						