@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Configuracion</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updateProfile') }}">
                        {!! csrf_field() !!}
                        {{Session::get('flash-message')}}
                       
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['name']}}" placeholder="Nombre">
                                <span class = "input-group-addon"><i class="fa  fa-user"></i></span>
                                </div>
                                
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label">Cedula</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="cedula" value="{{$data['cedula']}}" placeholder="Cedula">
                                <span class = "input-group-addon"><i class="fa  fa-user"></i></span>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Telefono</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="telefono" value="{{$data['telefono']}}" placeholder="Telefono">
                                <span class = "input-group-addon"><i class="fa  fa-phone"></i></span>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                          
                            <div class="col-md-6" id="map">
                                
                                
                            </div>
                       
                            
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Genero</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="genero" value="{{$data['genero']}}" placeholder="Genero">
                                <span class = "input-group-addon"><i class="fa  fa-user"></i></span>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Instagram</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['instagramUserName']}}" placerholder="cuenta de instagram" disabled>
                                <span class = "input-group-addon"><a href="{{ url('/addInstagram') }}"  ><i class="fa  fa-instagram"></i></a></span>
                                 <span class = "input-group-addon"><a href="{{ url('/deteleSocial/it') }}"  ><i class="fa  fa-close"></i></a></span>
                               
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Twitter</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="text" class="form-control" name="name" value="{{$data['twitterUserName']}}" disabled>
                                <span class = "input-group-addon"><a href="{{ url('/addTwitter') }}"  ><i class="fa  fa-twitter"></i></a></span>
                                <span class = "input-group-addon"><a href="{{ url('/deteleSocial/tw') }}"  ><i class="fa  fa-close"></i></a></span>
                               
                                 </div>
                                
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label class="col-md-4 control-label">Categoria</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                 <span class = "input-group-addon"><a href="{{url('/newTagU')}}">Nueva Categoria<i class="fa  fa-tag"></i></a></span>
                                </div>
                                
                            </div>
                           
                        </div>
                         <div class="form-group" style="color:black;">
                             <div class="col-md-6">
                            @foreach ($data['tags'] as $key => $tags)
                                <div class = "input-group col-md-6">
                                 
                               <input type="text" class="form-control " name="tag" ng-model="tag" value=" {{$tags->name}}">
                            
                                <span class = "input-group-addon"><a href="{{url('/deleteTagU')}}/{{$tags->id}}"><i class="fa  fa-close"></i></a></span>
                           
                                </div> 
                            @endforeach
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Guardar
                                </button>
                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
