@extends('layouts.app')

@section('content')
<div class="container" >
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

               
                <div class="panel-body">
                    Administrativo
                    <ul>
                        <li>
                    <a href="{{url('/calificationtypeList')}}">Lista de Calificaciones</a>
                </li>
                   <li>
                    <a href="{{url('/tagList')}}">Lista de etiquetas</a>
                    </li>
                     </ul>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
