<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-social.css')}}" rel="stylesheet">
    <link href="{{asset('css/ng-tags-input.css')}}" rel="stylesheet">
    <link href="{{asset('css/angular-input-stars.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/angular.ng-notify/0.6.0/ng-notify.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.14.9/select.css" rel="stylesheet">
    
     <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://select2.github.io/dist/js/select2.full.js" rel="stylesheet"></script>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
    <script src="{{asset('js/jquery-validate.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/angular-validate.min.js')}}" rel="stylesheet"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/angular.ng-notify/0.6.0/ng-notify.min.js"></script>
  
    <script src="{{asset('js/ngAutocomplete.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/ng-tags-input.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/controllers/app.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/angular-input-stars.js')}}" rel="stylesheet"></script>
     
      <script src="{{asset('js/select2.js')}}" rel="stylesheet"></script>

<div class="container text-center">
		<div class="logo-404">
			<a href="/" class="navbar-2"><span class="orange">Com</span><span class="white">praka</span></a>
		</div>
		<div class="content-404">
			<img src="images/404/404.png" class="img-responsive" alt=""  height="350" style="height: 350px;"/>
			<h1><b>OPPS!</b> No podemos encontrar esta ruta</h1>
			<h2><a href="/">Ve a inicio otra vez</a></h2>
		</div>
	</div>
