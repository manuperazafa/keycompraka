@extends('layouts.app')

@section('content')

    
  
    
            <div class="container">
                <span us-spinner spinner-key="spinner-1" spinner-start-active="false" us-spinner="{radius:30, width:8, length: 16}"></span>
                <div class="row">
                    <div class="col-md-12 padding-right">
                        <div class="features_items" >
                            <!--features_items-->
                        
                            <div class="col-md-3" ng-repeat="accounts in userAccounts">
                                <div class="product-image-wrapper" style="border : 0;">
                                    <div class="single-products">
                                        <a href="{{ url('/getUserInstagram') }}/<%accounts.id%>">
                                            <div class="productinfo text-center" style="background-image : url('<%accounts.logo%>');border-radius: 5px 5px 0 0; display-inline:block; background-size: 100% 100%;
    background-repeat: no-repeat;">
                                                <i class="fa " id="fauser" style="background-color : transparent;"></i>
                                                <a class="pull-right m-fa-r" href="#"></a>
                                                
                                                 </div>
                                        </a>
                                    </div>

                                    <div class="choose" style="background-color: #24272D;color: #fff; text-align : center;">

                                        <ul class="nav nav-pills nav-justified" style="height: 80px;overflow: hidden;     padding-top: 12px;font-size: 30px;">
                                            <%accounts.instagramUserName%>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                                      
                            <!--features_items-->
                        </div>
                    </div>
                </div>
            </div>
    
@endsection
