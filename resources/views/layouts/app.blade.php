<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>compraka</title>

    <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="https://select2.github.io/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-social.css')}}" rel="stylesheet">
    <link href="{{asset('css/ng-tags-input.css')}}" rel="stylesheet">
    <link href="{{asset('css/angular-input-stars.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/angular.ng-notify/0.6.0/ng-notify.min.css" rel="stylesheet">
   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.14.9/select.css" rel="stylesheet">
    <link href="{{asset('css/angular-input-stars.css')}}" rel="stylesheet">
    
     <!-- JavaScripts -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    
    <script src="https://select2.github.io/dist/js/select2.full.js" rel="stylesheet"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
    
    
    <script src="{{asset('js/jquery-validate.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/angular-validate.min.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/bootstrap.js')}}" rel="stylesheet"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    -->
    <script src="https://cdn.jsdelivr.net/angular.ng-notify/0.6.0/ng-notify.min.js"></script>
    <script src="{{asset('js/moment-with-locales.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/moment-angular.js')}}" rel="stylesheet"></script>
    
    <script src="{{asset('js/ngAutocomplete.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/ngAutocomplete.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/ng-tags-input.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/controllers/app.js')}}" rel="stylesheet"></script>
    <script src="{{asset('js/angular-input-stars.js')}}" rel="stylesheet"></script>

   <!--   <script type="text/javascript" src="http://fgnass.github.io/spin.js/spin.min.js"></script>-->
  <script type="text/javascript" src="{{asset('js/spin.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/angular-spinner.js')}}"></script>
     
      <script src="{{asset('js/select2.js')}}" rel="stylesheet"></script>








<script>

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 9
  });
  
    google.maps.event.trigger(map, 'resize');
  // Try HTML5 geolocation.
  if (navigator.geolocation) {

    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      var lat = document.getElementById('lat').value;
      var lon = document.getElementById('lng').value;
      if(lat != "" && lon != ""){
        pos.lat = document.getElementById('lat');
        pos.lng = document.getElementById('lng');
      }
      else {
        document.getElementById('lat').value = pos.lat;
        document.getElementById('lng').value = pos.lng;
      }
     
      map.setCenter(pos);
      var marker = new google.maps.Marker({
        position: pos,
        map: map,
        draggable : true,
        title: 'Hello World!'
      });
       google.maps.event.addListener(marker, "dragend", function(ev){
                var evv = ev.latLng;
                var lat = evv.lat();
                var lng = evv.lng();
                document.getElementById('lat').value = lat;
                document.getElementById('lng').value = lng;
                
                
                 
        });
    }, function() {
       
      handleLocationError(true, infoWindow, map.getCenter());
    });

   

  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }


}



</script>   

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVx84I3_HJHvfl_CLV4InFxAslcfFHNpU&signed_in=true&callback=initMap"></script>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

    </style>
    
</head>
<body id="app-layout" ng-app="comprakaApp" ng-controller="comprakaCtrl">

<header id="header">
        <!--header-->
        </div>
        <!--/header_top-->
        <div class="header-middle">
            <!--header-middle-->
            <div class="container" id="no_padding_top" style="width: 100% !important;">
                <div class="row">
                    <div class="col-md-3" id="">
                        <div class="logo pull-left">
                            <a href="/" class="navbar-2"><span class="yellow">Com</span><span class="black">praka</span></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="btn-group">
                            <div class="col-md-3 " style="    padding-left: 0;">
                              
                                <select class="dropdown-toggle "  ui-select2 id="categorias" on-select="search()" ng-model="categorias" ng-change="search()" ng-options="tag.id as tag.name for tag in tags" >
                                 <option value="">Todas</option>
                                </select>
                         
                               
                            </div>              
                            <div class="col-md-3" style="padding-right: 0; padding-left:0;margin-left: -40px;">   
                                <div class = "input-group">
                                <input type="text" class="form-control" ng-model="searchUser" name="name"  placeholder="Nombre">
                                <span  ng-click="search()" class = "input-group-addon" style="background-color : #FFDF66; border-radius : 0;"><i class="fa  fa-search"></i></span>
                            </div> 
                        </div>

                        @if (Auth::guest())
                        <div class="shop-menu col-sm-6">
                            <ul class="nav navbar-nav">
                                <li style="margin-top : 5px;">
                                                                              <i class="fa fa-remove" style="color:red;" ng-click="calificacion.calificacion = 0 ; search();"></i>
         
                                                  <input-stars max="5" min="0" icon-base="fa fa-fw fa-1x" icon-empty="fa-star-o" icon-hover="hover" icon-full="fa-star" ng-model="calificacion.calificacion" ng-click="search();" ></input-stars>
                        
                                </li>
                                <!--li><ol class="reputation-scale"><li class="level-1">Rojo</li><li class="level-2">Naranja</li><li class="level-3">Amarillo</li><li class="level-4">Verde claro</li><li class="level-5 selected"><strong>Verde</strong></li></ol></li-->
                                <li><a href="#" data-toggle="modal" data-target="#sigInModal">Ingresar</a></li>
                                <li><a href="#" id="line">|</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#signUpModal">Regístrate</a></li>
                            </ul>
                        </div>

                        <!--Modal de Ingresar-->

                        <div class="modal fade" id="sigInModal" role="dialog">
                            <div class="modal-dialog" id="sin-modal">
                                <!-- Modal contenido-->
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" name="loginForm">
                             {!! csrf_field() !!}    
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Ingresar en mi cuenta</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <input type="email" name="email" value="" placeholder="ejemplo@dominio.com" class="form-control" ng-model="user.email">
                                            
                                              <span class="error" ng-show="loginForm.email.$error.email">Correo Invalido</span>

                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" value="" placeholder="Contraseña" class="form-control" ng-model="user.password" ng-minlength="8">
                                            <span class="error" ng-show="loginForm.password.$error.minlength">Debe tener minimo 8 carateres</span>             
                                        </div>
                                        
                                        <div class="pull-right">
                                            <a href="#" data-toggle="modal" data-target="#renemberModal" class="pointer-hand" data-dismiss="#signUpModal" ng-click="closeM2()">Olvidé mi contraseña</a>
                                        </div>
                                        </br>
                                    </div>
                                    <div class="modal-footer">
                                        
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default col-lg-12"  style="background-color: #FFDF66;font-size: 20px;font-weight: bold; ">Ingresar</button>
                                        </div>
                                        <div class="form-group">
                                            <a href="{{ url('/loginFacebook') }}" class="btn btn-block btn-social btn-facebook " style="width: 45%;float: left;margin: 0;" ><i class="fa fa-btn fa-facebook" style="margin-right: 0;"></i>Facebook</a>
                               
                                        
                                            <a href="{{ url('/signUpGoogle') }}" class="btn btn-block btn-social btn-google " style="width: 45%;float: right;margin: 0;"><i class="fa fa-btn fa-google" style="margin-right: 0;"></i>Google</a>
                                        
                                        </div>
                                    </div>
                                </div>
                                </form>
                                
                            </div>
                        </div><!--Modal de Ingresar-->
                        <!-- Modal Recordar Contraseña-->
                        <div id="renemberModal" class="modal fade" role="dialog">
                            <div class="modal-dialog" id="sin-modal">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Recuperar Contraseña</h4>
                                    </div>
                                    <form class="form-horizontal" name="forgotForm" role="form" method="POST" action="{{ url('/password/email') }}">
                                    {!! csrf_field() !!}
                                    <div class="modal-body">
                                        <p>Ingresa tu email para recuperar contraseña</p>
                                        <div class="form-group">
                                            <input type="text" name="email" ng-model="emailForgot" value="" placeholder="ejemplo@dominio.com" class="form-control">
                                            <span class="error" ng-show="forgotForm.email.$error.email">Correo no valido</span>
                                        </div>
                                    </div>
                                    
                                    <div class="modal-footer text-center" style="padding: 1px 45px 30px 45px;">
                                        
                                        <button type="submit" class="btn btn-default w-100"  >Continuar</button>
                                         <p></p>
                                          <p></p>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div><!-- /Modal Recordar Contraseña-->
                        <!--Modal de Registrarse-->
                        <div class="modal fade" id="signUpModal" role="dialog">
                            <div class="modal-dialog" id="sin-modal">
                                <!-- Modal contenido-->
                                <div class="modal-content p-b">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Registrate</h4>
                                    </div>
                                    <div class="modal-body">
                                      <form name="registerForm" method="POST">  
                                        <div class="form-group">
                                            <input type="mail" name="email" value="" placeholder="ejemplo@dominio.com" class="form-control" ng-model="userNew.email" >
                                            <span class="error" ng-show="registerForm.email.$error.email">Correo Invalido</span>

                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="input-password" value="" placeholder="Contraseña" class="form-control" ng-model="userNew.password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="input-password" value="" placeholder="Repetir Contraseña" class="form-control" ng-model="userNew.password2">
                                        </div>
                                      </form>  
                                    </div>

                                    <div class="modal-footer text-center m-t">
                                        <div class="form-group">
                                        <button type="button" class="btn btn-default w-100"  ng-click="register();">Registrame</button>
                                        </div>
                                        <div class="form-group">
                                            <a href="{{ url('/loginFacebook') }}" class="btn btn-block btn-social btn-facebook " style="width: 45%;float: left;margin: 0;" ><i class="fa fa-btn fa-facebook" style="margin-right: 0;"></i>Facebook</a>
                               
                                        
                                            <a href="{{ url('/signUpGoogle') }}" class="btn btn-block btn-social btn-google " style="width: 45%;float: right;margin: 0;"><i class="fa fa-btn fa-google" style="margin-right: 0;"></i>Google</a>
                                        
                                        </div>
                                   
                                        
                                    </div>
                                    <div class="text-center">
                                    </br>
                                            <p>¿Ya tienes una cuenta?<a href="#" data-toggle="modal" data-target="#sigInModal" data-dismiss="#signUpModal" ng-click="closeM();">Ingresar</a></p>
                                    </div>
                                </div>
                            </div>
                        </div><!--/Registrarse-->
                        @else
                        <div class="shop-menu col-sm-6" >
                            <ul class="nav navbar-nav">
                                <li style="margin-top : 5px;">
                                                  <input-stars max="5" icon-base="fa fa-fw fa-1x" icon-empty="fa-star-o" icon-hover="hover" icon-full="fa-star" ng-model="calificacion.calificacion" ng-click="search();" ></input-stars>
         
                                </li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"  id="user-link"><i class="fa fa-user" id="fauser-3"></i>{{Auth::user()->name}}</a>
                                 

                                    <ul class="dropdown-menu" role="menu" style="padding-left: 15px;">
                                        @if(Auth::user()->user_type == 1)
                                        <li><a href="/tagList" data-toggle="modal"><i class="fa fa-btn fa-wrench"></i>Categorias</a></li>
                                        <li><a href="/calificationtypeList" data-toggle="modal"><i class="fa fa-btn fa-wrench"></i>Calificaciones</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#updatePassword"><i class="fa fa-btn fa-key"></i>Cambiar Contraseña</a></li>
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>   
                                        @else
                                        <li><a href="{{url('/mycalifications')}}"><i class="fa fa-btn fa-star"></i>Calificaciones Recibidas</a></li>
                                        <li><a href="{{url('/fromCalification')}}"><i class="fa fa-btn fa-star"></i>Calificaciones hechas</a></li>
                                        
                                        <li><a href="#" data-toggle="modal" data-target="#configModal"  ng-click="profile();"><i class="fa fa-btn fa-wrench"></i>Configuracion</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#updatePassword"><i class="fa fa-btn fa-key"></i>Cambiar Contraseña</a></li>
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>
                                        @endif
                                    </ul>
                                </li>
                                 </ul>
                        </div>

 <!--Modal de config-->
                        <div class="modal fade" id="configModal" role="dialog">
                            <div class="modal-dialog" id="config-modal">
                                <!-- Modal contenido-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title" id="modal-title-body">Configuración</h4>
                                    </div>
                                    <div class="modal-body" id="modal-config-body">
                                        <div class="form-group">
                                            <input type="text" name="input-name" ng-model="userProfile.name" value="" placeholder="Nombre" class="form-control">
                                        </div>
                                     
                                        <div class="form-group">
                                            <input type="text" name="input-ci" ng-model="userProfile.cedula" value="" placeholder="Cédula" class="form-control">
                                        </div>
                                       
                                      <div id="mapG" class="map">
                                          <div class="col-md-12" id="map" style="height: 400px;">
                                
                                
                                           </div>
                                         
                                        
                                        </div>
                                        <div class="form-group">
                                            <input type="text" style="display: none;" name="input-lat" id="lat" ng-model="userProfile.latitud" value="">
                                            <input type="text" style="display: none;" name="input-lng" id="lng" ng-model="userProfile.longitud" value="">
                                        </div>
                                        <div class="form-group">
                                            <select name="input-genero"     ng-model="userProfile.genero" class="form-control">
                                                <option value="">Seleccione</option>
                                                <option value="masculino">Masculino</option>
                                                <option value="femenino">Femenino</option>
                                           
                                             </select>
                                        </div>
                                        <div class="form-group">
                                         <div class = "input-group">
                                <input type="text" class="form-control" name="name"  placeholder="Usuario de Instagram" id="instagramId" ng-model="userProfile.instagramUserName" ng-change="cuentaAgregada()" disabled>
                                <span class = "input-group-addon"><i class="fa  fa-instagram" ng-click="agregarInstagram();"></i></span>
                                 <span class = "input-group-addon"><i class="fa  fa-close" ng-click="deleteSocial('it')"></i></span>
                               
                                </div>
                                        </div>
                                        <div class="form-group">
                                                 <tags-input ng-model="userProfile.tags" add-on-paste="true" class="bootstrap"  min-tags="3"
                  max-tags="5" placeholder="categorias" add-from-autocomplete-only="true " display-property="name" key-property="id">
        <auto-complete source="loadTags()" on-tag-removed="tagRemoved($tag)" load-on-focus="true"
                     load-on-empty="true"></auto-complete>
    </tags-input>
                                        </div>
                                        <div class="modal-footer text-center" id="modal-footer-config">
                                            <button type="button" class="btn btn-default w-30" style="    margin-bottom: 20px;" data-dismiss="modal" ng-click="updateProfile()">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <!-- Modal Recordar Contraseña-->
                        <div id="updatePassword" class="modal fade" role="dialog">
                            <div class="modal-dialog" id="sin-modal">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Cambiar Contraseña</h4>
                                    </div>
                                    <div class="modal-body">  
                                    <div class="form-group">
                                            <input type="password" name="input-renember" value="" ng-model="userPassword.password" placeholder="Nueva Contraseña" class="form-control">
                                    </div>
                                    <div class="form-group">
                                            <input type="password" name="input-renember" value="" ng-model="userPassword.password2" placeholder="Repita Nueva Contraseña" class="form-control">
                                    </div>
                                    <div class="modal-footer text-center">
                                        <button type="button" style="margin-bottom : 30px;"class="btn btn-default w-100" data-dismiss="modal" ng-click="passwordUpdate()">Continuar</button>
                                        <br>
                                    </div>
                                     <p></p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /Modal Recordar Contraseña-->
                    @endif
                    </div>
                </div>
            </div>
        </div>
        
    </header>

     @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>

    @endif
    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

    @yield('content')
   
   

</body>
</html>
