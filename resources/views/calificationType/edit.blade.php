@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tipos de Calificaciones</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updateCalificationtype') }}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <div class = "input-group">
                                <input type="hidden" name="id" value="{{$Calificationtype->id}}">
                                <input type="text" class="form-control" name="name" value="{{$Calificationtype->name}}">
                                <span class = "input-group-addon"><i class="fa  fa-tag"></i></span>
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Valor</label>
                        <div class="col-md-6">
                                  

                                <div class = "input-group">
                                <input type="number" class="form-control" name="value" value="{{$Calificationtype->value}}">
                                <span class = "input-group-addon"><i class="fa  fa-tag"></i></span>
                                </div>
                                
                         </div>

                        

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Guardar
                                </button>
                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
