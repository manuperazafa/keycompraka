@extends('layouts.app')

@section('content')
<div class="container" style="background-color : white; margin-top : 40px;  border-radius: 5px;">
  <h2><a href="{{url('/newCalificationType')}}">Añadir nueva</a></h2>
  <h2>Calificaciones</h2>

   <table class="table">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Valor</th>
        <th colspan="2">Acciones</th>

      </tr>
    </thead>
    <tbody>
      
     @foreach ($Calificationtype as $key => $Calificationtypes)
							
							<tr>
				       
				        <td>{{$Calificationtypes->name}}</td>
                 <td>{{$Calificationtypes->value}}</td>
				        <td><a href="{{url('/editCalificationtype')}}/{{$Calificationtypes->id}}">Editar</a></td>
				        <td><a href="{{url('/deleteCalificationtype')}}/{{$Calificationtypes->id}}">Eliminar</a></td>
         
             </tr>
						@endforeach	
    </tbody>
  </table>
</div>


@endsection						