var comprakaApp = angular.module('comprakaApp', ['ngTagsInput',  'angular-input-stars' , 'ui.select2', 'angularMoment','angularSpinner' , 'ngNotify'], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
});
comprakaApp.controller('comprakaCtrl', function ($scope , $http ,  $window , usSpinnerService , ngNotify) {
 	 
       		 	
       		 	$scope.tags = [];
       		 	$scope.user = {};
       		 	$scope.userNew = {};
       		 	$scope.userProfile= {};
       		 	$scope.userAccounts = {};
				$scope.calificacion = {};
				$scope.userPassword = {};
				$scope.calificacionListType = {};

				$http({
				  method: 'GET',
				  url: '/getTags'
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $scope.tags = response.data;

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
				$http({
				  method: 'GET',
				  url: '/getAccounts'
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $scope.userAccounts = response.data;

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
				
				$scope.users = [];
				$http({
				  method: 'GET',
				  url: '/getUsers'
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $scope.users = response.data;

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });


                $http({
				  method: 'GET',
				  url: '/CalificationtypeList2'
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $scope.calificacionListType = response.data;
				  

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
          $scope.closeM = function(){
          	$("#signUpModal").hide();
          }
          $scope.closeM2 = function(){
          	$(".modal-backdrop").remove();
          	$("#sigInModal").hide();
          }

          
          $scope.recuperarContra = function(){
          	$http({
				  method: 'POST',
				  url: '/resetPassword',
				  data : {'email' : $scope.emailForgot},
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
          }
          $scope.search = function(){

          	usSpinnerService.spin('spinner-1');
          	if(!$scope.searchUser){
          		$scope.searchUser = "";
          	}
          	if(!$scope.categorias){
          		$scope.categorias = "";
          	}
          	if(!$scope.calificacion.calificacion){
          		$scope.calificacion.calificacion = "";
          	}
          	
          	$http({
				  method: 'GET',
				  url: '/getAccounts?name='+$scope.searchUser+'&categoria='+$scope.categorias+'&calificacion='+$scope.calificacion.calificacion
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				     usSpinnerService.stop('spinner-1');
				    if(response.data){
				    	$scope.userAccounts = response.data;
				    }
				    else {
				    	$scope.userAccounts = response.data;
				    	ngNotify.set('No hay coincidencias', 'success');
				    }
				    

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				      ngNotify.set('No hay coincidencias', 'success');

				  });
          }

          $scope.login = function(){
          	if(!$scope.user.email){
            	alert('campos vacios');
            	//$scope.userNew.email('background-color' , 'red');
            }
            else if (!$scope.user.password || !$scope.userNew.password2){
            	alert('campos vacios');
            }
            else {
          	$http({
				  method: 'POST',
				  url: '/authenticate',
				  data : $scope.user,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
			}	
          }

          $scope.register = function(){
            if(!$scope.userNew.email){
            	//alert('campos vacios');
            	ngNotify.set('Hay campos vacios', 'error');
            	//$scope.userNew.email('background-color' , 'red');
            }
            else if (!$scope.userNew.password || !$scope.userNew.password2){
            	ngNotify.set('Hay campos vacios', 'error');
            }
            else if ($scope.userNew.password |= !$scope.userNew.password2){
           		ngNotify.set('Las Contraseñas no coindicen', 'error');	
            }
            else {
          	$http({
				  method: 'POST',
				  url: '/register',
				  data : $scope.userNew,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $('#signUpModal').modal('hide');
				    ngNotify.set('Registrado Exitosamente', 'success');

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				    ngNotify.set('Error Interno', 'error');
				  });
			}	
          }

          $scope.profile = function(){

          	$http({
				  method: 'GET',
				  url: '/profile'
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				 
				    $scope.userProfile.name=response.data.name;
				    $scope.userProfile.cedula = response.data.cedula;
				    $scope.userProfile.genero = response.data.genero;
				    $scope.userProfile.latitud = response.data.latitud;
				    $scope.userProfile.longitud = response.data.longitud;
				    $scope.userProfile.tags = response.data.tags;
				    $scope.userProfile.instagramUserName= response.data.instagramUserName;
				   
				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
          }


          $scope.updateProfile = function(){
          	
          	$http({
				  method: 'POST',
				  url: '/updateProfile',
				  data : $scope.userProfile,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $('#configModal').modal('hide');
				    ngNotify.set('Datos Actualizados Exitosamente', 'success');

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
          }

          $scope.passwordUpdate = function(){
          	if (!$scope.userPassword.password || !$scope.userPassword.password2){
            	alert('campos vacios');
            }
            else if (!$scope.userPassword.password != !$scope.userPassword.password2){
            	alert('no coincidencias');
            }
            else {
          	$http({
				  method: 'POST',
				  url: '/updatePassword',
				  data : $scope.userPassword,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $('#configModal').modal('hide');
				    ngNotify.set('Datos Actualizados Exitosamente', 'success');

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
			}	
          }
          
                $scope.loadTags = function() {
                     


                     return $http.get('/getTags');
                   
                  


                };

                $scope.getCalifications = function(param){
                	
                $http({
				  method: 'GET',
				  url: '/getCalifications/'+param
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    moment.locale('es'); 
				    $scope.calificacionList = response.data;

				    //var date  = moment(response.data.updated_at).format('MMMM Do YYYY, h:mm:ss a');
				    //$scope.calificacionList.updated_at = date;
				    //console.log(date);
				  	
				
				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
                }

                $scope.deleteSocial = function(param) {
                     
                		$http({
				  method: 'GET',
				  url: '/deleteSocial/'+param,
				  data : $scope.userPassword,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				    $scope.userProfile.instagramUserName = "";
				    ngNotify.set('Eliminado Exitosamente', 'success');
				  

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
                
                   
                  	


                };
                
                $scope.deleteTag = function(tag) {
                     

                     console.log(1);
                   
                  


                };

                $scope.cuentaAgregada = function(){
                	 ngNotify.set('Agregada Exitosamente', 'success');
                }



                $scope.calificar = function(idUser) {
                     
          		$scope.calificacion.userId = idUser;
                     
               $http({
				  method: 'POST',
				  url: '/addCalification',
				  data : $scope.calificacion,
				  headers: { 'Content-Type' : 'application/x-www-form-urlencoded'}
				}).then(function successCallback(response) {
				    // this callback will be called asynchronously
				    // when the response is available
				   	$scope.getCalifications(idUser);
				    ngNotify.set('Calificacion Exitosa', 'success');

				  }, function errorCallback(response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
				  });
                  


                };
                
                 $scope.agregarInstagram = function() {
                 	$scope.ventana = $window.open('http://test-proyecto4.com.ve/addInstagram', 'new window', 'width=500,height=400');
                 };

                 $scope.agregarTwitter = function() {
                 	$scope.ventana = $window.open('http://www.test-proyecto4.com.ve/addTwitter', 'new window', 'width=500,height=400');
                 };
                  $scope.tab = 1;	
          
});