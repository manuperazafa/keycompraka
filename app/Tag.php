<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
     protected $table = 'tags';
	 protected $fillable = ['name',  'enabled'];
	 protected $guarded = ['id'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }
}
