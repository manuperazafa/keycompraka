<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calification extends Model
{
    //
     protected $table = 'califications';
	 protected $fillable = ['user_id', 'user_target_id' , 'comentario','calificacion'];
	 protected $guarded = ['id'];

}
