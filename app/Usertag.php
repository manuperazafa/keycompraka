<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usertag extends Model
{
    //
     protected $table = 'user_tags';
	 protected $fillable = ['user_id', 'tag_id'];
	 protected $guarded = ['id'];
}
