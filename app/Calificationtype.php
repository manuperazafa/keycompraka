<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificationtype extends Model
{
    //
     protected $table = 'calification_type';
	 protected $fillable = ['name','value', 'enabled'];
	 protected $guarded = ['id'];
}
