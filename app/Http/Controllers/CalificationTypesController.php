<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calificationtype;
use Auth;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CalificationTypesController extends Controller
{
    
    function __construct()
    {
         $this->middleware('auth');
    }
  
    public function addCalificationtype(Request $request){

        
    	$Calificationtype = Calificationtype::create([
	            'name' => $request->input('name'),
                'value' => $request->input('value'),
                'enabled' => true
         
	     ]);
         Session::flash('message', 'Registro Agregado');
      
         return redirect('/calificationtypeList');
    
    }

     public function deleteCalificationtype($idCalificationtype){


           $Calificationtype = Calificationtype::find($idCalificationtype);
           $Calificationtype->enabled = false;
           $Calificationtype->save();   
             
        
             Session::flash('message', 'Registro Eliminado');
        
         return redirect('/calificationtypeList');
    
    }

    public function updateCalificationtype(Request $request){
        $Calificationtype = Calificationtype::find($request->input('id')); 
        $Calificationtype->name = $request->input('name');
        $Calificationtype->value = $request->input('value');
        $Calificationtype->save();
        Session::flash('message', 'Registro Actualizado');
        return redirect('/calificationtypeList');
    }

    public function editCalificationtype($idCalificationtype){
        $Calificationtype = Calificationtype::find($idCalificationtype); 
        return view('calificationType.edit')->with('Calificationtype', $Calificationtype);
    }

     public function CalificationtypeList(Request $request){
        $Calificationtype = Calificationtype::where('enabled', true)->get();
        
      
         return view('calificationType.list')->with('Calificationtype',$Calificationtype);
    
    }

    public function CalificationtypeList2(Request $request){
        $Calificationtype = Calificationtype::where('enabled', true)->get();
        
      
         return json_encode($Calificationtype);
    
    }

   
}
