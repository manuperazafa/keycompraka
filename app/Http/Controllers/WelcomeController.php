<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tag;
use App\Calificationtype;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    //
    function welcome(Request $request){

    	if($request->get('name')){
    		$data = User::where('instagramUserName', $request->get('name'))->get();
    	}
    	else if($request->get('categoria')){
            $data = User::where('instagramId', '>', 0)->get();
    		
    	}
        else if($request->get('calificacion')){
            $data = User::where('instagramId', '>', 0)->get();
        }    
    	else {
    		$data = User::where('instagramId', '>', 0)->get();
    	}
    	$tag = Tag::where('enabled', true)->get();
        $Calificationtype = Calificationtype::where('enabled', true)->get();
        
    	//return view('welcome')->with('data' , $data)->with('tags' , $tag)->with('Calificationtype', $Calificationtype);
        return view('welcome');
           
    }

    function getAccounts(Request $request){

        if($request->get('name')){
           // $data = User::where('instagramUserName', $request->get('name'))->get();
         
            $data = DB::table('users') 
            ->select('*')
            ->where('instagramUserName', 'like', '%'.$request->get('name').'%')
            ->where('instagramId', '>', 0)->get();
        }
        else if($request->get('categoria')){


            $data = DB::table('users')
            ->join('user_tags', 'users.id', '=', 'user_tags.user_id')    
            ->select('*')
            ->where('users.instagramId', '>', 0)
            ->where('user_tags.tag_id', '=', $request->get('categoria'))->get();
        } 
        else if($request->get('calificacion')){


            $data = DB::table('users') 
            ->select('*')
            ->where('users.instagramId', '>', 0)
            ->where('users.promedio', '=', $request->get('calificacion'))->get();
        }    
        else {
            $data = User::where('instagramId', '>', 0)->get();
        }
        
        //return view('welcome')->with('data' , $data)->with('tags' , $tag)->with('Calificationtype', $Calificationtype);
        return json_encode($data);
           
    }

    

     public function getTags(){
        $tag = Tag::where('enabled', true)->get();
        
        
         return json_encode($tag);
    
    }
}
