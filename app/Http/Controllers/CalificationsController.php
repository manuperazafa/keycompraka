<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calification;
use App\Calificationtype;

use Auth;
use Session;
use DB;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CalificationsController extends Controller
{
    

    public function addCalification(Request $request){

    	$inputs = $request->json()->all();
        
        $calif = Calification::where('user_id', Auth::user()->id)->where('user_target_id', $inputs['userId'])->first();
        if($calif){
             $calif->comentario = $inputs['comentario'];
             $calif->calificacion = $inputs['calificacion'];
             $calif->save();
             
        }
        else {

        	$calification = Calification::create([
    	            'user_id' => Auth::user()->id,
    	            'user_target_id' => $inputs['userId'],
    	            'comentario' => $inputs['comentario'],
    	            'calificacion' => $inputs['calificacion']
    	           
    	     ]);
        }
        $promedio = DB::table('califications')
                ->where('user_target_id', $inputs['userId'])
                ->avg('calificacion');

        $user = User::find($inputs['userId']);        
        $user->promedio = $promedio;
        $user->save();
      

    	//Session::flash('message', 'Usuario Calificado exitosamente');
    	return 1;
    	//return redirect('/getUserInstagram/'.$request->input('userId'));
    }

    public function myCalification(){


    	//$data = Calification::where('user_target_id', '=', Auth::user()->id)->get();
    	$data = DB::table('users')
            ->join('califications', 'users.id', '=', 'califications.user_id')
            ->join('calification_type', 'califications.calificacion', '=', 'calification_type.id')       
            ->select('users.*', 'califications.comentario' , 'califications.id as idComment' , 'calification_type.name as ncalificacion')
            ->where('user_target_id', '=', Auth::user()->id)->get();
        
       
		return view('users.calification')->with('data' , $data);
    }

    public function fromCalification(){


    	//$data = Calification::where('user_target_id', '=', Auth::user()->id)->get();
    	$data = DB::table('users')
            ->join('califications', 'users.id', '=', 'califications.user_id')
            ->join('calification_type', 'califications.calificacion', '=', 'calification_type.id')       
            ->select('users.*', 'califications.comentario' , 'califications.id as idComment' , 'calification_type.name as ncalificacion')
            ->where('user_id', '=', Auth::user()->id)->get();
        
       
		return view('users.calificationFrom')->with('data' , $data);
    }

    public function editCalification($idCalification){
    	$calificacion = Calification::find($idCalification); 
    	return view('editCalification')->with('data', $calificacion);
    }
    public function editCalificationSave(Request $request){
		$calificacion = Calification::find($request->input('id')); 
		$calificacion->comentario = $request->input('comentario');
		$calificacion->calificacion = $request->input('calificacion');
	
		$calificacion->save();
		Session::flash('message', 'Calificacion modificada');
		return redirect('/mycalifications');	
				   
	}

    public function updateCalification(Request $request){
		$calificacion = Calification::find($request->input('id')); 
		$calificacion->comentario = $request->input('comentario');
	    $calificacion->calificacion = $request->input('calificacion');
	
		$calificacion->save();
		Session::flash('message', 'Usuario Calificado exitosamente');
		return redirect('/getUserInstagram/'.$request->input('userId'));	
				   
	}

	public function getCalifications($idUser){
	
	
		   $data = DB::table('califications')
            ->join('calification_type', 'califications.calificacion', '=', 'calification_type.id')
            ->join('users', 'califications.user_id', '=', 'users.id')      
            ->select('users.name as username' , 'califications.*','calification_type.name')
            ->where('user_target_id', '=', $idUser)
            ->orderBy('califications.id', 'desc')
            ->get(); 
		return json_encode($data);
	}
}
