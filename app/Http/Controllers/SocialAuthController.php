<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Tag;
use App\Calification;
use App\Calificationtype;
use Validator;
use Auth;
use Session;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
class SocialAuthController extends Controller
{


	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';



    public function loginWithFacebook(Request $request)
	{
	    // get data from request
	    $code = $request->get('code');

	    // get fb service
	    $fb = \OAuth::consumer('Facebook');

	    // check if code is valid

	    // if code is provided get user data and sign in
	    if ( ! is_null($code))
	    {
	        // This was a callback request from facebook, get the token
	        $token = $fb->requestAccessToken($code);

	        // Send a request with it
	        $result = json_decode($fb->request('/me?fields=id,name,email'), true);

	        //$message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
	        //echo $message. "<br/>";

	        $user = User::where('email', '=', $result['email'])->first();
			if ($user === null) {
			   $user = User::create([
	            'name' => $result['name'],
	            'email' => $result['email'],
	            'facebookId' => $result['id'],
	            'password' => bcrypt($result['id']),
	        	]);
			   	
			   
          	 	 Auth::login($user);

    			 if (Auth::check()) {
				        return redirect('/'); //change this part to anywhere you wish to be redirected to
				  }
			}
			else {

				
				 Auth::login($user);


   				  if (Auth::check()) {
				        return redirect('/'); //change this part to anywhere you wish to be redirected to
				  }
								 
			}
	    }
	    // if not ask for permission first
	    else
	    {
	        // get fb authorization
	        $url = $fb->getAuthorizationUri();

	        // return to facebook login url
	        return redirect((string)$url);
	    }
	}


	
	public function signUpWithGoogle(Request $request)
	{
	    // get data from request
	    $code = $request->get('code');

	    // get google service
	    $googleService = \OAuth::consumer('Google');

	    // check if code is valid

	    // if code is provided get user data and sign in
	    if ( ! is_null($code))
	    {
	        // This was a callback request from google, get the token
	        $token = $googleService->requestAccessToken($code);

	        // Send a request with it
	        $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

	        //$message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
	        $user = User::where('email', '=', $result['email'])->first();
			if ($user === null) {
			   $user = User::create([
	            'name' => $result['name'],
	            'email' => $result['email'],
	            'googleId' => $result['id'],
	            'password' => bcrypt($result['id']),
	        	]);
			   	
			   
          	 	 Auth::login($user);
          	 	
    			 if (Auth::check()) {
				        return redirect('/'); //change this part to anywhere you wish to be redirected to
				  }
			}
			else {

				
				 Auth::login($user);
				 

   				  if (Auth::check()) {
				        return redirect('/'); //change this part to anywhere you wish to be redirected to
				  }
								 
			}
	        
        	
	    }
	    // if not ask for permission first
	    else
	    {
	        // get googleService authorization
	        $url = $googleService->getAuthorizationUri();

	        // return to google login url
	        return redirect((string)$url);
	    }
	}



	public function getUserInstagram($userId)
	{
	     // get data from request

		$calification = array();
	    $user = User::where('id' , '=' ,  $userId)->first();
	    $calification = Calification::where('user_target_id' , '=' ,  $userId)->get();
	    if (Auth::check()) {
		    // The user is logged in...
	      $MyCalification = Calification::where('user_target_id' , '=' ,  $userId)->where('user_id' ,Auth::user()->id)->first();
	 
		}
		else {
			$MyCalification = array();
		}
	    $data = file_get_contents('https://api.instagram.com/v1/users/'.$user->instagramId.'/media/recent/?access_token=1474334019.87a11cf.0d0744ee2320455c869500b8fcb9a8e1');
		$data2 = json_decode(file_get_contents('https://api.instagram.com/v1/users/'.$user->instagramId.'/?access_token=1474334019.87a11cf.0d0744ee2320455c869500b8fcb9a8e1'));
		//dd($data);
		$user['followers'] = $data2->data->counts->follows;
		$user['followings'] = $data2->data->counts->followed_by;
		
		$user['profile_picture'] = $data2->data->profile_picture;
		$Calificationtype = Calificationtype::where('enabled', true)->get();
        
		$result = json_decode($data , true);
		
	    return view('instagram')->with('data',$result['data'])->with('user' , $user)->with('calification', $calification)->with('MyCalification' , $MyCalification)->with('Calificationtype',$Calificationtype); 
	    
	}


	public function addInstagram(Request $request)
	{
	     	
	 	 // get data from request
	    $code = $request->get('code');

	    // get instagram service
	    $instagramService = \OAuth::consumer('Instagram');
	    
	     // if code is provided get user data and sign in
	    if ( ! is_null($code))
	    {
	        // This was a callback request from facebook, get the token
	        $token = $instagramService->requestAccessToken($code);

	        // Send a request with it
	       
	        $result = json_decode($instagramService->request('users/self'), true);
	        //echo 'Your unique instagram user id is: ' . $result['data']['id'] . ' and your name is ' . $result['data']['full_name'];
	        	    	    
	           $user = User::where('instagramId', '=', $result['data']['id'])->first();
	         
				if ($user == null) {
				  	
				 
				   $user = User::find(Auth::user()->id); 
				   $user->instagramId = $result['data']['id'];
				   $user->instagramUserName = $result['data']['username'];
				   $user->logo = $result['data']['profile_picture'];
				   $user->save();	
				   

					echo  "<script type='text/javascript'>";
					echo "window.opener.document.getElementById('instagramId').value='".$result['data']['username']."';";
					echo "window.close();";
					echo "</script>";
			
				}
				else {

					
				   echo  "<script type='text/javascript'>";
					//echo "window.opener.document.getElementById('instagramId').value='Value changed..';";
					echo "window.close();";
					echo "</script>";
									 
				}
	    }
	    // if not ask for permission first
	    else
	    {
	        // get fb authorization
	        $url = $instagramService->getAuthorizationUri();

	        // return to facebook login url
	        return redirect((string)$url);
	    }

	  
	    
	}

	public function addTwitter(Request $request)
	{
	    // get data from request
	    $token  = $request->get('oauth_token');
	    $verify = $request->get('oauth_verifier');

	    // get twitter service
	    $tw = \OAuth::consumer('Twitter');
	    
	    // check if code is valid

	    // if code is provided get user data and sign in
	    if ( ! is_null($token) && ! is_null($verify))
	    {
	        // This was a callback request from twitter, get the token
	        $token = $tw->requestAccessToken($token, $verify);

	        // Send a request with it
	        $result = json_decode($tw->request('account/verify_credentials.json'), true);

	        //$message = 'Your unique Twitter user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
	        $user = User::where('twitterId', '=', $result['id'])->first();
			if ($user === null) {
			      //$user = Session;	
				   //return $user;	
				   $user = User::find(Auth::user()->id);
				   $user->twitterId = $result['id'];
				   $user->twitterUserName = $result['screen_name'];
				   $user->save();	
		     	   $user['tags'] = Tag::where('user_id', '=', Auth::user()->id)->get();
       
				   
				   //return $data->instagramId;
			echo  "<script type='text/javascript'>";
					echo "window.opener.document.getElementById('twitterId').value='".$result['screen_name']."';";
					echo "window.close();";
					echo "</script>";
			
				}
				else {

					
				   echo  "<script type='text/javascript'>";
					//echo "window.opener.document.getElementById('instagramId').value='Value changed..';";
					echo "window.close();";
					echo "</script>";
									 
				}
	    }
	    // if not ask for permission first
	    else
	    {
	        // get request token
	        $reqToken = $tw->requestRequestToken();

	        // get Authorization Uri sending the request token
	        $url = $tw->getAuthorizationUri(['oauth_token' => $reqToken->getRequestToken()]);

	        // return to twitter login url
	        return redirect((string)$url);
	    }
	}

	public function deleteSocial($socialNetwork){
		  
		$user = User::find(Auth::user()->id); 
				   
		if($socialNetwork == 'tw'){

		  $user->twitterId = '';
		  $user->twitterUserName = '';
		  	  
				   
		}
		elseif($socialNetwork == 'it'){

		   $user->instagramUserName = '';
		    $user->instagramId = '';	
		}
		$user->save();
		//Session::flash('message', 'Cuenta Eliminada Exitosamente');
		
		return 1;	
				   
	}


}
