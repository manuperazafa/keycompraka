<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Usertag;
use Auth;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserTagsController extends Controller
{
    
    function __construct()
    {
         $this->middleware('auth');
    }
    public function newTag(Request $request){
        $tag = Tag::all();
        
      
         return view('users.tag')->with('tag',$tag);
    
    }
    public function addTag(Request $request){
    	$tag = Usertag::create([
	            'tag_id' => $request->input('tag'),
	            'user_id' => Auth::user()->id
	           
	     ]);
         Session::flash('message', 'Categoria Agregada');
      
         return redirect('/profile');
    
    }

     public function deleteTag($idTag){


           $tag = Usertag::find($idTag);
           $tag->delete();  
             
        
         Session::flash('message', 'Categoria Eliminada');
        
         return redirect('/profile');
    
    }
}
