<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Tag;
use App\Usertag;
use App\Calificationtype;
use Validator;
use Auth;
use DB;
use Password;
use Mail;
use Session;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\ResetsPasswords;
class UserController extends Controller
{   

   
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    

    public function showProfile()
    {   
         $data = Auth::user();
         //$data['tags'] = Usertag::where('user_id', '=', Auth::user()->id)->get();
         $data['tags'] = DB::table('tags')
            ->join('user_tags', 'tags.id', '=', 'user_tags.tag_id')      
            ->select('tags.name' , 'user_tags.id')
            ->where('user_tags.user_id', '=', Auth::user()->id)->get();
         //return view('users.profile')->with('data', $data);
         return json_encode($data);
    }

    public function updateProfile(Request $request)
    {   

         $user = User::find(Auth::user()->id);
         $inputs = $request->json()->all();
         $usertag = Usertag::where('user_id', Auth::user()->id)->delete();
       
         foreach ($inputs['tags'] as $key => $value) {
             Usertag::create([
                'tag_id' => $value['id'],
                'user_id' => Auth::user()->id
               
             ]);
         }
         $user->name= $inputs["name"];
         $user->cedula = $inputs["cedula"];
         $user->latitud = $inputs["latitud"];
         $user->longitud = $inputs["longitud"];
         $user->genero = $inputs["genero"];
         
         
         $user->save();   
         //Session::flash('message', 'Perfil Actualizado Exitosamente');
         //return redirect('/profile');
         return 1;

    }
    
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        Session::flash('message','You have successfully verified your account.');

        return redirect('/home');
    }

     public function updatePassword(Request $request)
    {   
        $inputs = $request->json()->all();
        $password = $inputs["password"];
         
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($password);
        $user->save();
        //Session::flash('message', 'Contraseña Actualizada Exitosamente');
        return 1;
    }

    function getUsers(Request $request){

        if($request->get('name')){
            $data = User::where('instagramUserName', $request->get('name'))->get();
        }
        else if($request->get('categoria')){
            $data = User::where('instagramId', '>', 0)->get();
            
        }
        else if($request->get('calificacion')){
            $data = User::where('instagramId', '>', 0)->get();
        }    
        else {
            $data = User::where('instagramId', '>', 0)->get();
        }
        $tag = Tag::where('enabled', true)->get();
        $Calificationtype = Calificationtype::where('enabled', true)->get();
        
        //return view('welcome')->with('data' , $data)->with('tags' , $tag)->with('Calificationtype', $Calificationtype);
        return json_encode($data);
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {

        
        $inputs = $request->json()->all();
        $email = $inputs["email"];
        $password = $inputs["password"];
         
     
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect('/');
        }
        else {
            return 0;
        }
    }

    public function register(Request $request)
    {   

        $inputs = $request->json()->all();
        $error = array('error' => '');
        $email = $inputs["email"];
        $password = bcrypt($inputs["password"]);
        
        $data = User::where('email', $email)->get();

        $confirmation_code = str_random(30);
        $user = User::create([
            'email' => $email,
            'password' => $password,
            'confirmation_code' => $confirmation_code
        ]);
        

        $confirmation_code = str_random(30);
        $datae = array( 'confirmation_code' => $confirmation_code);
        $email = Mail::send('email.confirm', $datae, function($message) use ($email){
            $message->to($email,  $email)
                ->subject('Verify your email address');
        });
      
        return $user;

    }

        /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request)
    {
        
         $inputs = $request->json()->get('email');
         //dd($inputs);
     $response = Password::sendResetLink($request->only('email'), function (Message $message) {
        $message->subject($this->getEmailSubject());
      });

    switch ($response) {
        case Password::RESET_LINK_SENT:
            return redirect()->back()->with('status', trans($response));

        case Password::INVALID_USER:
            return redirect()->back()->withErrors(['email' => trans($response)]);
    }

    }


}