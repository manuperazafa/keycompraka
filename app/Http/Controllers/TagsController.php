<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Auth;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
    
    function __construct()
    {
         $this->middleware('auth');
    }
  
    public function addTag(Request $request){
    	$tag = Tag::create([
	            'name' => $request->input('name'),
                'enabled' => true,
	            'user_id' => Auth::user()->id
	           
	     ]);
         Session::flash('message', 'Categoria Agregada');
      
         return redirect('/tagList');
    
    }

     public function deleteTag($idTag){


           $tag = Tag::find($idTag);
                   $tag->enabled = false;
                   $tag->save();   
             
        
             Session::flash('message', 'Categoria Eliminada');
        
         return redirect('/tagList');
    
    }

    public function updateTag(Request $request){
        $tag = Tag::find($request->input('id')); 
        $tag->name = $request->input('name');
        $tag->save();
        Session::flash('message', 'Categoria Actualizada');
        return redirect('/tagList');
    }

    public function editTag($id){
        $tag = Tag::find($id); 
        return view('editTag')->with('tag', $tag);
    }

    public function tagList(Request $request){
        $tag = Tag::where('enabled', true)->get();
        
      
         return view('taglist')->with('tag',$tag);
    
    }

   
}
