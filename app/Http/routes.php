<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


/*
	Rutas Redes Sociales
*/


Route::group(['middleware' => ['auth']], function () {
    //
    
});

Route::get('auth/logout', 'Auth\AuthController@logout');
Route::get('/register/verify/{confirmation_code}', 'UserController@confirm');
    
Route::post('/resetPassword' , 'UserController@sendResetLinkEmail');

Route::group(['middleware' => ['web' , 'cors']], function () {
    Route::auth();
    Route::get('/', 'WelcomeController@welcome');

    Route::get('/getAccounts', 'WelcomeController@getAccounts');
    
    Route::get('/getUsers', 'UserController@getUsers');
    Route::post('/authenticate' , 'UserController@authenticate');
    Route::post('/register' , 'UserController@register');
    
    Route::get('/home', 'HomeController@index');
    Route::post('/addTag', 'TagsController@addTag');
    Route::get('/deleteTag/{idTag}', 'TagsController@deleteTag');
    Route::get('/newTag', 'TagsController@newTag');
    Route::get('/tagList', 'TagsController@tagList');
    Route::get('/editTag/{id}', 'TagsController@editTag');
    Route::post('/updateTag', 'TagsController@updateTag');

    Route::post('/addCalificationtype', 'CalificationTypesController@addCalificationtype');
    
    Route::get('/CalificationtypeList2', 'CalificationTypesController@CalificationtypeList2');
    
    Route::get('/deleteCalificationtype/{idCalificationtype}', 'CalificationTypesController@deleteCalificationtype');
    Route::get('/calificationtypeList', 'CalificationTypesController@CalificationtypeList');
    Route::get('/editCalificationtype/{idCalificationtype}', 'CalificationTypesController@editCalificationtype');
    Route::post('/updateCalificationtype', 'CalificationTypesController@updateCalificationtype');


    Route::get('/newTagU', 'UserTagsController@newTag');
    Route::post('/addTagU', 'UserTagsController@addTag');
    Route::get('/deleteTagU/{idTag}', 'UserTagsController@deleteTag');
    
    Route::post('/addCalification', 'CalificationsController@addCalification');
    Route::get('/getCalifications/{idUser}', 'CalificationsController@getCalifications');
    
    
    Route::post('/editCalificationSave', 'CalificationsController@editCalificationSave');
    Route::post('/updateCalification', 'CalificationsController@updateCalification');
    Route::get('/mycalifications', 'CalificationsController@myCalification');
    Route::get('/editCalification/{idCalification}', 'CalificationsController@editCalification');
    Route::get('/fromCalification', 'CalificationsController@fromCalification');
   
    Route::get('/getTags', 'WelcomeController@getTags');

    Route::get('/profile', 'UserController@showProfile');
    Route::post('/updateProfile', 'UserController@updateProfile');
    Route::get('/getUserInstagram/{userId}', 'SocialAuthController@getUserInstagram');
    Route::get('/signUpGoogle', 'SocialAuthController@signUpWithGoogle');
	Route::get('/loginFacebook', 'SocialAuthController@loginWithFacebook');
	Route::get('/loginTwitter', 'SocialAuthController@loginWithTwitter');
	Route::get('/loginInstagram', 'SocialAuthController@loginWithInstagram');
	Route::get('/addInstagram', 'SocialAuthController@addInstagram');
	Route::get('/addTwitter', 'SocialAuthController@addTwitter');
	Route::post('/updatePassword', 'UserController@updatePassword');
    Route::get('/deleteSocial/{socialNetwork}' , 'SocialAuthController@deleteSocial');
    Route::get('/changepassword', function () {
    //
    return view('password');
    });
    Route::get('/newTag', function () {
    //
       return view('tag');
    });
    Route::get('/newCalificationType', function () {
    //
       return view('calificationType.type');
    });
    
    

   
       


});




