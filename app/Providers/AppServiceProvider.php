<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tag;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
            //$tag  = Tag::get();
            
            //View::share('tags', $tag);
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
